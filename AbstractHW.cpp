#include <iostream>
#include <vector>

class Point
{
public:
	Point()
	{}

	Point(int x, int y, int z)
		: m_x(x), m_y(y), m_z(z)
	{}

private:
	int m_x;
	int m_y;
	int m_z;
};

class Wheel
{
public:

	Wheel()
	{}

	Wheel(float Diametr)
		: m_D(Diametr)
	{}

	float GetDiametr() const { return m_D; }
private:
	float m_D = 10;
};

class Engine
{
public:
	Engine()
	{}

	Engine(float Power)
		: m_Power(Power)
	{}

	float GetPower() const { return m_Power; }

private:
	float m_Power = 100;
};

class Vehicle
{
public:
	virtual std::ostream& print(std::ostream& out) const = 0;
	friend std::ostream& operator<<(std::ostream& out, const Vehicle& v)
	{
		return v.print(out);
	}
protected:
	Vehicle() {};
};

class Circle : public Vehicle
{
public:
	Circle(Point p, int radius)
	{
		m_P = p;
		m_R = radius;
	}

	virtual std::ostream& print(std::ostream& out) const override
	{
		out << "Water Circle, radius: " << m_R;
		return out;
	}

protected:
	Point m_P;
	int m_R;
};

class WaterVehicle : public Vehicle
{
public:
	WaterVehicle(float WaterLine)
		: m_WaterLine(WaterLine)
	{}

	virtual std::ostream& print(std::ostream& out) const override
	{
		out << "Water Vehicle, waterline: "  << m_WaterLine;
		return out;
	}

	float GetWaterLine() const { return m_WaterLine; }
private:
	float m_WaterLine;
};

class RoadVehicle : public Vehicle
{
public:
	RoadVehicle(float RoadHeight)
		: m_RoadHeight(RoadHeight)
	{}

	float GetRoadHeight() const { return m_RoadHeight; }
private:
	float m_RoadHeight;
};

class Bicycle : public RoadVehicle
{
public:
	Bicycle(Wheel w1, Wheel w2, float RoadHeight = 100)
		: RoadVehicle(RoadHeight)
	{
		m_Wheels.push_back(w1);
		m_Wheels.push_back(w2);
	}

	virtual std::ostream& print(std::ostream& out) const override
	{
		out << "Bicycle Wheels: ";
		for (const auto& w : m_Wheels)
			out << w.GetDiametr() << ' ';

		out << "Ride height: " << GetRoadHeight();
		return out;
	}
private:
	std::vector<Wheel> m_Wheels;
};

class Car : public RoadVehicle
{
public:
	Car(Engine eng, Wheel w1, Wheel w2, Wheel w3, Wheel w4, float RoadHeight = 100)
		: RoadVehicle(RoadHeight)
	{
		m_Engine = eng;
		m_Wheels.push_back(w1);
		m_Wheels.push_back(w2);
		m_Wheels.push_back(w3);
		m_Wheels.push_back(w4);
	}

	virtual std::ostream& print(std::ostream& out) const override
	{
		out << "Car Engine: " << m_Engine.GetPower() << ' ';
		out << "Wheels: ";
		for (const auto& w : m_Wheels)
			out << w.GetDiametr() << ' ';

		out << "Ride height: " << GetRoadHeight();
		return out;
	}

	float GetEnginePower() const
	{
		return m_Engine.GetPower();
	}

private:
	std::vector<Wheel> m_Wheels;
	Engine m_Engine;
};

float getHighestPower(const std::vector<Vehicle*>& v)
{
	float max_val = -1;
	for (const auto& v_ptr : v)
	{
		Car* c = dynamic_cast<Car*>(v_ptr);
		if (c != nullptr && c->GetEnginePower() > max_val)
		{
			max_val = c->GetEnginePower();
		}
	}

	return max_val;
}

int main()
{
	// TODO 1:
	//  Car Engine : 150 Wheels : 17 17 18 18 Ride height : 150
	//	Bicycle Wheels : 20 20 Ride height : 300
	Car c(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 150);
	std::cout << c << '\n';

	Bicycle t(Wheel(20), Wheel(20), 300);
	std::cout << t << '\n';

	// TODO 2:

	std::vector<Vehicle*> v;

	v.push_back(new Car(Engine(150), Wheel(17), Wheel(17), Wheel(18), Wheel(18), 250));
	v.push_back(new Circle(Point(1, 2, 3), 7));
	v.push_back(new Car(Engine(200), Wheel(19), Wheel(19), Wheel(19), Wheel(19), 130));
	v.push_back(new WaterVehicle(5000));

	//TODO: ����� ��������� ������� v �����

	std::cout << "The highest power is " << getHighestPower(v) << '\n'; // ���������� ��� �������

	//TODO: �������� ��������� ������� v �����
	for (auto& v_ptr : v)
	{
		delete v_ptr;
	}

	return 0;

}